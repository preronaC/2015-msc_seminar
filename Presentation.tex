\documentclass{beamer}
\usepackage{amssymb,hyperref}
\usetheme{PaloAlto}
\useoutertheme{infolines}
\begin{document}

\mode<presentation>
\title{Compactness Theorem for Propositional Logic:}
\subtitle{A Topological Proof\\}
\author{Prerona Chatterjee\\(Roll No.: 142123029)}
\date{9th October 2015}
\institute{Department of Mathematics \\IIT Guwahati}

\begin{frame}
	\titlepage
\end{frame}

\AtBeginSection[]
{
\begin{frame}
	\tableofcontents[currentsection]
\end{frame}
}

\section{Introduction}
\begin{frame}
\begin{block}{Introduction}
How far is it from computer science to topology? Surprisingly, it is not far at all.  \\
This presentation brings forward the closeness of topology to computer science by proving the The Compactness Theorem of Propositional Logic using Topological Methods, which following Tarski gives the theorem its name.
\end{block} \pause
\begin{block}{Importance}
Compactness is one of the central notions of logic - as Jerome Keisler put it, ‘The most useful theorem in model theory is probably the compactness theorem’ (1965, p. 113) - yet it has received relatively little philosophical attention.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Compactness Theorem for Propositional Logic}
\textbf{Let $\mathcal{P}$ be a set of propositional constants, and $\mathcal{W}$ be an infinite set of formulas over $\mathcal{P}$.\\
If every finite subset of $\mathcal{W}$ is satisfiable, so is $\mathcal{W}$.}
\end{block}
\end{frame}

\section{Understanding the Statement}

\begin{frame}
\begin{block}{Proposition}
A proposition or statement is a sentence which is either true or false.
\end{block} \pause
\begin{block}{Propositional Constant}
A propositional constant represents some particular proposition.
\end{block} \pause
\begin{block}{Truth Value of a Proposition}
If a proposition is true, then we say its truth value is true, and if a proposition is false, we say its truth value is false.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Logical Connectives}
\begin{itemize}
\item $\neg$ denotes 'not' or negation: If P is a proposition constant, then $\neg$P is true if P is false, and is false if P is true.
\item $\vee$ denotes 'or' or disjunction: If P, Q are propositional constants, then P $\vee$ Q is false if both P and Q are false, and true otherwise.
\item $\wedge$ denotes 'and' or conjunction: If P, Q are propositional constants, then P $\wedge$ Q is true if both P and Q are true, and false otherwise.
\item $\rightarrow$ denotes 'conditional' or implication: If P and Q are propositional constants, the conditional statement P $\rightarrow$ Q is false when P is true and Q is false, and is true
otherwise. 
\item $\leftrightarrow$ denotes 'biconditional': If P and Q are propositional constants, the biconditional statement P $\leftrightarrow$ Q, is true if P and Q have the same truth values, and false otherwise.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Well-Formed Formula over a set of Propositional Constants}
Let $\mathcal{P}$ be a set of propositional constants. We define Well-Formed formulas (or simply Formula) over $\mathcal{P}$ as follows:
\begin{itemize}
\item P is a formula $\forall$P $\in$ $\mathcal{P}$.
\item If $\phi$ is a formula, then $\neg\phi$ is also a formula.
\item If $\phi_{1}$ and $\phi_{2}$ are formulas, then ($\phi_{1}$ $\vee$ $\phi_{2}$), ($\phi_{1}$ $\wedge$ $\phi_{2}$), \\($\phi_{1}$ $\rightarrow$ $\phi_{2}$) and ($\phi_{1} \leftrightarrow \phi_{2}$) are also formulas.
\item Nothing else is a formula.
\end{itemize}
\end{block} \pause
\begin{block}{Note}
A formula can only contain a finite sequence of propositional constants from $\mathcal{P}$ even though $\mathcal{P}$ can be arbitrary.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Truth Assignments}
Let $\mathcal{P}$ be a set of propositional constants.\\
A truth assignment is a function $f$ : $\mathcal{P}$ $\rightarrow$ \{$True$, $False$\}.
\end{block} \pause
\begin{block}{Models}
Let $\mathcal{P}$ be a set of propositional constants.\\
A model for $\mathcal{P}$ is a function that assigns to each P $\in$ $\mathcal{P}$ a proposition (i.e. meaning).\\
We can also view a model as a truth assignment because ultimately we work only with the truth values of the proposition corresponding to the propositional constants and not the proposition itself.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Satisfiablity}
Let $\mathcal{P}$ be a set of propositional constants and let $\mathcal{W}$ be a set of formulas over $\mathcal{P}$.
\begin{itemize}
\item $\phi$ $\in$ $\mathcal{W}$ is satisfiable if it is possible to find a truth assignment that makes the formula true.
\item $\mathcal{W}$ is satisfiable if it is possible to find a truth assignment that makes $\phi$ true $\forall\phi$ $\in$ $\mathcal{W}$.
\item $\mathcal{W}$ is finitely satisfiable if given any finite $\mathcal{W}'$ $\subseteq$ $\mathcal{W}$,\\ $\mathcal{W}'$ is satisfiable.
\end{itemize}
\end{block} \pause
\begin{block}{Validity}
Let $\mathcal{P}$ be a set of propositional constants, and let $\phi$ be a formula over $\mathcal{P}$.
\begin{itemize}
\item $\phi$ is a tautology if all interpretations make $\phi$ true.
\item $\phi$ is a contradiction if no interpretation makes $\phi$ true.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Compactness Theorem for Propositional Logic}
\textbf{Let $\mathcal{P}$ be a set of propositional constants, and $\mathcal{W}$ be an infinite set of formulas over $\mathcal{P}$.\\
If every finite subset of $\mathcal{W}$ is satisfiable, so is $\mathcal{W}$.}
\end{block}
\end{frame}

\section{What exactly do we need?}

\begin{frame}
\begin{block}{What we need and what we have...}
\textbf{What we need:} A truth assignment that makes $\phi$ true\\ $\forall\phi$ $\in$ $\mathcal{W}$. \\
\textbf{What we have:} A truth assignment that makes $\phi$ true\\ $\forall\phi$ $\in$ $\mathcal{W}'$ given any finite $\mathcal{W}'$ $\subseteq$ $\mathcal{W}$
\end{block} \pause
\begin{block}{That looks similar...}
If we try and recall the definition of Compactness in Topology, we realise that we have something very similar: \\
Given a topological space (X, $\mathcal{T}$), X is said to be compact iff each of its open covers has a finite sub-cover or alternately:\\ \pause
Given a topological space (X, $\mathcal{T}$), X is said to be compact iff for every collection of closed subsets $\mathcal{C}$ of X, any finite $\mathcal{C}' \subseteq \mathcal{C}$ has non empty intersection $\Rightarrow$ $\mathcal{C}$ has non empty intersection.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Searching for the correct Topological Space}
So, to use compactness, here is what we need: \pause
\begin{itemize}
\item $\mathcal{C}$ must be a collection of sets, each of whose members are truth assignments because it is a truth assignment that we require in its intersection. \pause
\item As we need a truth assignment that makes $\phi$ true $\forall\phi \in \mathcal{W}$, \\$\mathcal{C}$ must be a collection of the sets \{X$_\phi$\}$_{\phi \in \mathcal{W}}$ where\\
X$_\phi$= The set of all truth assignments that make $\phi$ true \pause
\item As X$_\phi$ must be members of the topology we will be considering, each X$_\phi$ must be a subset of the set we start with. Hence, the set we should consider is the set of all truth assignments on $\mathcal{P}$ \pause
\item The topological space constructed must be compact \pause
\item The topological space must be such that X$_\phi$ is closed $\forall\phi \in \mathcal{W}$
\end{itemize}
\end{block}
\end{frame}

\section{Constructing a Topological Space}

\begin{frame}
\begin{block}{Defn: Topology on a Set}
Let X be a set. A topology on X is a collection $\mathcal{T}$ of subsets of X satisfying: \\
\begin{itemize}
\item $\mathcal{T}$ contains $\emptyset$ and X \\
\item $\mathcal{T}$ is closed under arbitrary unions \\
(i.e. if U$_{i}$ $\in$ $\mathcal{T}$ $\forall$i $\in$ I for an arbitrary indexing set I, then $\bigcup_{i \in I}$ U$_{i}$ $\in$ $\mathcal{T}$)\\
\item $\mathcal{T}$ is closed under finite intersections \\
(i.e. if U$_{1}$, U$_{2}$ $\in$ $\mathcal{T}$ then U$_{1}$ $\cap$ U$_{2}$ $\in$ $\mathcal{T}$)
\end{itemize}
\end{block} \pause
\begin{block}{\underline{Remark}}
One may view the first condition as a special case of the other two since $\emptyset$ is a union of the empty collection and X is the intersection of the empty (hence finite) collection.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defn: Topological Space}
A topological space (X,$\mathcal{T}$) is a set X together with a topology $\mathcal{T}$ on it. Normally we denote the topological space by X instead of (X,$\mathcal{T}$).
\end{block} \pause
\begin{block}{Defn: Open Sets, Closed Sets and Neighbouhoods}
Now, let (X,$\mathcal{T}$) be a Topological Space. 
\begin{itemize}
\item The elements of $\mathcal{T}$ are called open subsets of X. 
\item A subset of X is said to be closed if its complement in X is open.  
\item A neighbourhood of p is a set U $\in$ $\mathcal{T}$ with p $\in$ U.
\end{itemize}
\end{block} \pause
\begin{block}{Defn: Basis for a Topological Space}
Let (X,$\mathcal{T}$) be a Topological Space.\\
$\mathcal{B}$ is called a basis for $\mathcal{T}$ if $\forall$G $\in$ $\mathcal{T}$, G can be written as an arbitrary union of elements of $\mathcal{B}$.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defn: Basis for some Topology on a Set}
If only a set X is given, a basis for some topology on X is a collection $\mathcal{B}$ of subsets of X (called basis elements) satisfying the following properties: 
\begin{itemize}
\item For each x ${\in}$ X, $\exists$ B ${\in}$ $\mathcal{B}$ such that x ${\in}$ B.
\item Let B$_{1}$, B$_{2}$ ${\in}$ $\mathcal{B}$. If x ${\in}$ B$_{1}$ $\cap$ B$_{2}$, then $\exists$ B ${\in}$ $\mathcal{B}$ such that x ${\in}$ B and B $\subseteq$ B$_{1}$ $\cap$ B$_{2}$.
\end{itemize}
\underline{Note}: The collection of all arbitrary unions of members of $\mathcal{B}$ forms a topology on X and is denoted by $\mathcal{T}_{\mathcal{B}}$
\end{block} \pause
\begin{block}{Defn: Sub-basis for some topology on a Set}
If X is a set, any collection $S$ of subsets of X form a sub-basis for some topology on X. \\
The topology induced by $S$ is the arbitrary union of finite intersections of members of $S$.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defining the Set}
Let $\mathcal{P}$ be a non-empty set of propositional constants and let $\mathcal{W}$ be the set of all formulas over $\mathcal{P}$. Consider the space of truth assignments, $\mathcal{V}$ = $\{v$ : $\mathcal{P}$ $\rightarrow$ $\{True$, $False\}\}$. \\ \pause
For each $v$ $\in$ $\mathcal{V}$, define a function $\overline{v}$ : $\mathcal{W}$ $\rightarrow$ \{$True$, $False$\} by 
$$
\overline{v}(\phi) = \left\{
        \begin{array}{ll}
            True & \quad v$ makes $\phi$ true $\\
            False & \quad v$ makes $\phi$ false
        $\end{array}
    \right.
$$ \pause
For each $\phi$ $\in$ $\mathcal{W}$, define X$_\phi$ = $\{v$ $\in$ $\mathcal{V}$ : $\overline{v}$($\phi$) = $True\}$.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Basis for a topology on the Set}
The set $\mathcal{B}$ = \{X$_\phi$ : $\phi$ $\in$ $\mathcal{W}$\} is a basis for a topology on $\mathcal{V}$.
\end{block} \pause
\begin{block}{Proof of the fact that $\mathcal{B}$ is a basis}
\begin{itemize}
\item As $\mathcal{P}$ $\neq$ $\emptyset$, $\exists$ P $\in$ $\mathcal{P}$. Define $\phi$ = P $\vee$ $\neg$P. \\
Then $\phi$ $\in$ $\mathcal{W}$ is a tautology \\
$\Rightarrow$ X$_\phi$ = $\mathcal{V}$ $\Rightarrow$ $\{$X$_\phi$ : $\phi$ $\in$ $\mathcal{W}$\} covers $\mathcal{V}$. \pause
\item Let $\alpha$, $\beta$ $\in$ $\mathcal{W}$ and let $v$ $\in$ X$_\alpha$ $\cap$ X$_\beta$. \\
Then, $\overline{v}$($\alpha$), $\overline{v}$($\beta$) = $True$. \\
Consider $\gamma$ = $\alpha \wedge \beta$.\\
Then $\overline{v}$($\gamma$) = $True$ and so, $v$ $\in$ X$_\gamma$.\\
Moreover, $\forall$ $v^{'}$ $\in$ X$_\gamma$, $\overline{v^{'}}$($\gamma$) = $True$ \\
$\Rightarrow$ $\overline{v^{'}}$($\alpha$), $\overline{v^{'}}$($\beta$) = $True$ \\
$\Rightarrow$ $v^{'}$ $\in$ X$_\alpha$ $\cap$ X$_\beta$ \\
and so, X$_\gamma$ $\subseteq$ X$_\alpha$ $\cap$ X$_\beta$.
\end{itemize}
\end{block}
\end{frame}

\section{An equivalent Topological Space}

\begin{frame}
\begin{block}{Defn: Continuous map}
Let (X, $\mathcal{T}$) and (Y, $\mathcal{T}'$) be two topological spaces, and \\ $f$: X $\rightarrow$ Y be a map. $f$ is said to be a continuous map if any one of the following hold:
\begin{itemize}
\item  $f^{-1}$(G) $\in$ $\mathcal{T}$ $\forall$G $\in$ (Y, $\mathcal{T}'$)
\item $f^{-1}$(F) is closed in $\mathcal{T}$ $\forall$F closed in (Y, $\mathcal{T}'$)
\item $f^{-1}$(B) $\in$ $\mathcal{T}$ $\forall$ basic open sets B, of (Y, $\mathcal{T}'$)
\item $f^{-1}$(S) $\in$ $\mathcal{T}$ $\forall$ sub-basic open sets S of (Y, $\mathcal{T}'$)
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defn: Open and Closed Maps}
Let (X, $\mathcal{T}$) and (Y, $\mathcal{T}'$) be two topological spaces, and \\ $f$: X $\rightarrow$ Y be a map.
\begin{itemize}
\item $f$ is said to be an open map if $f$(B) $\in$ $\mathcal{T}'$ $\forall$ basic open set B of (X, $\mathcal{T}$).
\item $f$ is said to be a closed map if $f$(F) is closed in $\mathcal{T}'$ $\forall$F closed in (X, $\mathcal{T}$).
\end{itemize}
\end{block} \pause
\begin{block}{Defn: Homeomorphism}
Let (X, $\mathcal{T}$) and (Y, $\mathcal{T}'$) be two topological spaces, and \\ $f$: X $\rightarrow$ Y be a map. $f$ is said to be a homeomorphism map if:
\begin{itemize}
\item $f$ is bijective \\
\item $f$ is continuous \\
\item $f$ is an open map (i.e. $f^{-1}$ is a continuous map).
\end{itemize}
In this case, (X, $\mathcal{T}$) and (Y, $\mathcal{T}'$) are said to be homoemorphic.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defn: Arbitrary Product of Sets}
Let $\Lambda$ be an arbitrary non-empty indexing set, and (X$_\alpha$, $\mathcal{T}_\alpha$) be topological spaces $\forall\alpha$ $\in$ $\Lambda$. \\
Define X = $\prod_{\alpha \in \Lambda}$X$_\alpha$ = $\{$x : $\Lambda$ $\rightarrow$ $\bigcup_{\alpha \in \Lambda}$ X$_\alpha$, such that \\
x($\alpha$) $\in$ X$_\alpha$ $\forall\alpha$ $\in$ $\Lambda\}$.
\end{block} \pause
\begin{block}{Defn: Projection Maps on the Product Set}
Define $\pi_\beta$ : X $\rightarrow$ (X$_\beta$, $\mathcal{T}_\beta$) to be the $\beta^{th}$ projection map defined by $\pi_\beta$(x) = x($\beta$).
\end{block} \pause
\begin{block}{Defn: The Product Topology}
The product topology $\mathcal{T}_p$ on X is the smallest topology on X which makes $\pi_\beta$ continuous $\forall\beta$ $\in$ $\Lambda$. \\
So, $\mathcal{T}_p$ is a topology on X having \\
\{$\pi_\beta^{-1}$(G$_\beta$) such that G$_\beta$ $\in$ $\mathcal{T}_\beta$\} as a sub-basis. \\
Thus a basis for $\mathcal{T}_p$ is $\prod_{\alpha \in \Lambda}$U$_\alpha$ where U$_\alpha$ are members of $\mathcal{T}_\alpha$ $\forall\alpha$ $\in$ $\Lambda$ and U$_\alpha$ = X$_\alpha$ for all but finitely many $\alpha$ $\in$ $\Lambda$.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Result: Projection Maps are Continuous}
By the very definition of the topology on X, $\pi_\beta$ is continuous $\forall\beta$ $\in$ $\Lambda$
\end{block} \pause
\begin{block}{Defn: Continuity of Maps with Product Spaces as Co-Domain}
Suppose:
\begin{itemize}
\item $\Lambda$ be an arbitrary non-empty indexing set
\item (X$_\alpha$, $\mathcal{T}_\alpha$) are topological spaces $\forall\alpha$ $\in$ $\Lambda$ and (X, $\mathcal{T}_p$) is the corresponding product topology 
\item (Y, $\mathcal{T}$) is any arbitrary topological space 
\item $f$ : (Y, $\mathcal{T}$) $\rightarrow$ (X, $\mathcal{T}_p$) is a map
\item $\pi_\alpha$ : X $\rightarrow$ (X$_\alpha$, $\mathcal{T}_\alpha$) is the $\alpha^{th}$ projection map $\forall\alpha \in \Lambda$\\
\end{itemize}
Then, $f$ is continuous iff $\pi_\alpha \circ f$ is continuous $\forall\alpha \in \Lambda$.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defn: Literals, Simple Product and Minterms}
Let $\mathcal{P}$ be a finite set of propositional constants.
\begin{itemize}
\item A propositional constant or its negation is called a literal.
\item A simple product is a conjunction of literals.
\item A minterm is a simple product in which for every statement variable, either the statement variable, or its negation appears exactly once.
\end{itemize} 
\end{block} \pause
\begin{block}{Defn: PDNF}
Let $\mathcal{P}$ be a finite set of propositional constants.\\
If $\mathcal{W}$ is the set of all formulas over $\mathcal{P}$, a formula $\phi$ $\in$ $\mathcal{W}$ is said to be in PDNF if it is a finite disjunction of minterms.
\end{block}
\begin{block}{Note:}
Any formula $\phi$ $\in$ $\mathcal{W}$ other than a contradiction has a unique PDNF representation.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{Defining a Map}
Consider the space X = $\prod_{P \in \mathcal{P}}$ $\{$0, 1$\}$ with the product topology, where \{0, 1\} has the discrete topology and consider the map $f$ : $\mathcal{V}$ $\rightarrow$ X defined by $f$($v$) = x where,
$$
\pi_P(x) = \left\{
        \begin{array}{ll}
            1 & \quad v$(P) = $True \\
            0 & \quad v$(P) = $False$
        $\end{array}
    \right.
$$ \\
if $\pi_P$ is the P$^{th}$ projection map.
\end{block} \pause
\begin{block}{Note}
 By the very definition, it is easy to see that $f$ is a bijection.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{$f$ is continuous}
We know that f is continuous iff $\pi_P \circ f$ is continuous $\forall$P $\in$ $\mathcal{P}$. So we take P $\in$ $\mathcal{P}$, and U to be any open set in \{0, 1\} with the discrete topology. \\
\begin{itemize}
\item[Case:] U = $\phi$ : ($\pi_P \circ f$)$^{-1}$(U) = $\phi$ and hence, is open in $\mathcal{V}$ \\ \pause
\item[Case:] U = \{0, 1\} : ($\pi_P \circ f$)$^{-1}$(U) = $\mathcal{V}$ and hence, is open in $\mathcal{V}$ \\ \pause
\item[Case:] U = \{1\} : ($\pi_P \circ f$)$^{-1}$(U) =  \{$v$ $\in$ $\mathcal{V}$ such that $v$(P) = $True$\} = X$_\alpha$ where $\alpha$ = P, and hence, is open in $\mathcal{V}$ \\ \pause
\item[Case:] U = \{0\} : ($\pi_P \circ f$)$^{-1}$(U) =  \{$v$ $\in$ $\mathcal{V}$ such that $v$(P) = $False$\} = X$_\alpha$ where $\alpha$ = $\neg$P, and hence, is open in $\mathcal{V}$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{$f$ is an open map}
Let $\alpha$ $\in$ $\mathcal{W}$. \\
If $\alpha$ is a contradiction, X$_\alpha$ = $\emptyset$ and hence, $f$(X$_\alpha$) = $\emptyset$ which is open. \\ \pause
Otherwise, $\alpha$ being a formula, $\exists$ $\mathcal{P}'$ = $\{$P$_1$, P$_2$, ..... P$_n\}$ $\subseteq$ $\mathcal{P}$, such that $\alpha$ is made up of only elements from $\mathcal{P}'$, and hence can be converted into a PDNF \\
So, we can write $\alpha$ as a finite disjunction of clauses, where each clause is a finite conjunction of propositional constants, and hence $\alpha$ will be true if and only if any one of the clauses is true. \\ \pause
Let there be k clauses.\\
Define for i $\in$ \{1, 2, .... k\}, and j $\in$ \{1, 2, ... n\},
$$
b_{ij} = \left\{
        \begin{array}{ll}
            1 & \quad$ P$_j$ appears un-negated in the i$^{th}$ clause $\\
            0 & \quad$ P$_j$ appears negated in the i$^{th}$ clause
        $\end{array}
    \right.
$$
\end{block}
\end{frame}

\begin{frame}
\begin{block}{$f$ is an open map (contd...)}
Now, $v$ $\in$ X$_\alpha$. \\
$\Rightarrow$ $v$ makes $\alpha$ true \\
$\Rightarrow$ $v$ makes atleast one of the clauses in $\alpha$ true \\
$\Rightarrow$ $\exists$ i $\in$ \{1, 2,....k\} such that, v(P) =
$$
 \left\{
        \begin{array}{ll}
            True$ if P = P$_j$ for some P$_j$ $\in$ \{P$_1$, ..... P$_n$\} and b$_{ij}$ = 1 $\\
            False $ if P = P$_j$ for some P$_j$ $\in$ \{P$_1$, ..... P$_n$\} and b$_{ij}$ = 0 $\\
            True$ or $False$ otherwise
        $\end{array}
    \right.
$$ \pause
Then,
\begin{center}
$f$(X$_\alpha$) = $f$\{$v$ $\in \mathcal{V}$ such that $v$ makes $\alpha$ true\}. \\
\[=\bigcup_{i=1}^k \left( \bigcap_{j = 1}^n \{\pi_{P_j}^{-1}(\{b_{ij}\})\} \right)\] \\
\end{center}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{$f$ is an open map (contd...)}
We note that \[\left( \bigcap_{j = 1}^n \{\pi_{P_j}^{-1}(\{b_{ij}\})\} \right)\]
forms a basis for X with the product topology $\forall$i $\in$ \{1, 2,....k\} \\
$\Rightarrow$ $f$(X$_\alpha$) is a finite union of basic open sets \\ $\Rightarrow$ $f$(X$_\alpha$) open \\
$\Rightarrow$ $f$ is an open map.
\end{block} \pause
\begin{block}{$\mathcal{V}$ and X are homeomorphic}
So, we see that $f$ is a homeomorphism and hence, $\mathcal{V}$ is homeomorphic to X
\end{block}
\end{frame}

\section{Compactness of the Topological Space}

\begin{frame}
\begin{block}{Defn: Cover, Sub-cover and Open Cover}
Let (X,$\mathcal{T}$) be a topological space.
\begin{itemize}
\item If $\mathcal{C}$ = \{U$_{i}$\}$_{i \in I}$ is an indexed family of sets U$_{i}$, then $\mathcal{C}$ is a cover of X if X $\subseteq$ $\bigcup_{i \in I}$\{U$_{i}$\}.
\item A subcover of $\mathcal{C}$ is a subset of $\mathcal{C}$ that still covers X. 
\item If U$_{i}$ $\in$ $\mathcal{T}$ $\forall$i $\in$ I, then $\mathcal{C}$ = \{U$_{i}$\}$_{i \in I}$ is called an open cover for X.
\end{itemize}
\end{block} \pause
\begin{block}{Defn: Compact Spaces}
Let (X, $\mathcal{T}$) be a topological space X. X is said to be compact if each of its open covers has a finite sub-cover.
\end{block} \pause
\begin{block}{Result: Compactness is preserved under Homeomorphism}
Let (X, $\mathcal{T}$), (Y, $\mathcal{T}'$) be topological spaces, \\
and $f$ : (X, $\mathcal{T}$) $\rightarrow$ (Y, $\mathcal{T}'$) be a homeomorphism. \\ A $\subseteq$ X is compact in (X, $\mathcal{T}$) iff $f$(A) is compact in (Y, $\mathcal{T}'$).
\end{block}
\end{frame}

\begin{frame}

\begin{block}{Result: The Tychonoff Theorem}
Given an arbitrary family \{X$_\alpha$\}$_{\alpha \in \Lambda}$ of compact spaces, their product X := $\prod _{\alpha \in \Lambda}$ X$_\alpha$ is compact.
\end{block} \pause
\begin{block}{The Topological Space we defined is Compact}
As $f$ is a homeomorphism, $\mathcal{V}$ is compact iff X is. \\
As \{0, 1\} with the discrete topology has finitely many open sets, it is compact. \\
$\Rightarrow$ By Tychonoff Theorem X is compact, and hence $\mathcal{V}$ is compact.
\end{block}
\end{frame}

\section{The Compactness Theorem of Propositional Logic}

\begin{frame}
\begin{block}{Defn: Finite Intersection Property}
Let X be a set, and let $\mathcal{A}$ = \{A$_{i}$\}$_{i \in I}$ be a family of subsets of X. Then, the collection $\mathcal{A}$ is said to have the finite intersection property (FIP), if any finite sub-collection J $\subseteq$ I has non-empty intersection (i.e. $\bigcap_{i \in J}$ A$_i$ $\neq$ $\emptyset$ $\forall$ finite J $\subseteq$ I).
\end{block} \pause
\begin{block}{Equivalent Definition of Compactness}
Let (X,$\mathcal{T}$) be a Topological Space. X is compact if and only if any collection of closed subsets of X with the finite intersection property has non empty intersection.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{X$_\alpha$ is closed $\forall\alpha$ $\in$ $\mathcal{W}$}
We note that as $f$ defined above is a homeomorphism, \\
$f$, $f^{-1}$ are well-defined continuous maps and hence, X$_\alpha$ is closed iff $f$(X$_\alpha$) is closed. \\ \pause
Now,
\[f(X_\alpha)=\bigcup_{i=1}^k \left( \bigcap_{j = 1}^n \{\pi_{P_j}^{-1}(\{b_{ij}\})\} \right)\] \\ \pause
As \{b$_{ij}$\} is closed $\forall$i, j and $\pi_{P_j}$ is continuous $\forall$P$_j$ $\in$ $\mathcal{P}'$, \\
$\pi_{P_j}^{-1}(\{b_{ij}\})$ is closed in X with the product topology. \\
$\Rightarrow$ $\bigcap_{j = 1}^n \{\pi_{P_j}^{-1}(\{b_{ij}\})\}$ is closed in X with the product topology. \\
$\Rightarrow$ $\bigcup_{i=1}^k \left( \bigcap_{j = 1}^n \{\pi_{P_j}^{-1}(\{b_{ij}\})\} \right)$ is closed in X with the product topology. \\
$\Rightarrow$ $f$(X$_\alpha$) is closed in X with the product topology. \\ $\Rightarrow$ X$_\alpha$ is closed in $\mathcal{V}$ with the topology defined above.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Compactness Theorem of Propositinal Logic}
Let $\mathcal{P}$ be a set of propositional constants, and let $\mathcal{W}$ be an infinite set of formulas over $\mathcal{P}$ such that every finite subset is satisfiable. \\
For each $\phi$ $\in$ $\mathcal{W}$, define C$_\phi$ = X$_\phi$ which is a closed set. \\
Consider the collection of closed sets $\mathcal{C}$ = \{C$_\phi$ : $\phi$ $\in$ $\mathcal{W}$\}. \pause
\begin{block}{$\mathcal{C}$ has the finite intersection property}
Consider any finite subcollection \{C$_{\phi_1}$,...., C$_{\phi_n}$\}. \\
By assumption, since \{$\phi_1$,...., $\phi_1$\} is a finite subset of $\mathcal{W}$, there is a truth assignment $v$ that satisfies each of these $\phi_i$. \\
Then $v$ $\in$ $\bigcap_{i=1}^n$ C$_{\phi_i}$, and so the intersection of any finite subcolleciton of $\mathcal{C}$ is nonempty.
\end{block} \pause
As $\mathcal{C}$ is compact, $\exists$ $v$ $\in$ $\bigcap_{C \in \mathcal{C}}$ C$_{\phi_i}$. \\
By definition, this $v$ satisfies $\phi$, $\forall\phi$ $\in$ $\mathcal{W}$, and hence $\mathcal{W}$ is satisfiable.
\end{block}
\end{frame}

\begin{frame}
\begin{block}{The Compactness Theorem for Propositional Logic}
Thus we have proved the Compactness Theorem for Propositional Logic which states:\\ 
\textbf{Let $\mathcal{P}$ be a set of propositional constants, and $\mathcal{W}$ be an infinite set of formulas over $\mathcal{P}$. If every finite subset of $\mathcal{W}$ is satisfiable, so is $\mathcal{W}$.}
\end{block}
\end{frame}

\begin{frame}
\begin{center}
\begin{huge}
\textbf{Thankyou}
\end{huge}
\end{center}
\end{frame}

\end{document} 